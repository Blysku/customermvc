package pl.kk.spring.ex6project.services;

import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.kk.spring.ex6project.models.BasicUserDetails;
import pl.kk.spring.ex6project.models.User;
import pl.kk.spring.ex6project.repositories.UserRepository;

@Service("jpaUserDetailsService")
public class JpaUserDetailsService implements UserDetailsService {

    private UserRepository repository;

    public JpaUserDetailsService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = repository.findById(userName).orElseThrow(() -> new UsernameNotFoundException(userName));
        return new BasicUserDetails(user);
    }
}
