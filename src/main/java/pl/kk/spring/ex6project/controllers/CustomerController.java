package pl.kk.spring.ex6project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.kk.spring.ex6project.exceptions.CustomerNotFoundException;
import pl.kk.spring.ex6project.models.Customer;
import pl.kk.spring.ex6project.repositories.CustomerRepository;

import java.util.List;

@Controller
@RequestMapping()
public class CustomerController {

    private CustomerRepository repository;

    @Autowired
    public CustomerController(CustomerRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/customers")
    public String homePage(Model model) {
        List<Customer> customers = repository.findAll();
        if (customers != null && !customers.isEmpty()) {
            model.addAttribute("customers", customers);
        }

        return "home";
    }

    @GetMapping("/customer/{id}")
    public String editCustomer(@PathVariable Long id, Model model, RedirectAttributes redirAttrs) {
        try {
            Customer customer = repository.findById(id).orElseThrow(CustomerNotFoundException::new);
            model.addAttribute("customer", customer);
        } catch (CustomerNotFoundException e) {
            e.printStackTrace();
            redirAttrs.addFlashAttribute("error", "Customer Not Found");
        }

        return "edit";
    }

    @GetMapping("/add")
    public String printAddPage(Model model) {
        model.addAttribute("customer", new Customer());

        return "edit";
    }

    @GetMapping("/edit/{id}")
    public String printEditPage(@PathVariable Long id, Model model) throws CustomerNotFoundException {
        Customer customer = repository.findById(id).orElseThrow(CustomerNotFoundException::new);

        model.addAttribute("customer", customer);

        return "edit";
    }

    @PostMapping("/post")
//    @Transactional(noRollbackFor = CustomerNotFoundException.class)
    public String editCustomer(Customer newCustomer, RedirectAttributes redirAttrs) {
        Customer customer;
        if (null != newCustomer.getId()) {
            customer = repository.findById(newCustomer.getId()).orElse(new Customer());
        } else {
            customer = new Customer();
        }

        if (null != newCustomer.getFirstName() && !newCustomer.getFirstName().isEmpty()) {
            customer.setFirstName(newCustomer.getFirstName());
        }

        if (null != newCustomer.getLastName() && !newCustomer.getLastName().isEmpty()) {
            customer.setLastName(newCustomer.getLastName());
        }

        if (null != newCustomer.getEmail() && !newCustomer.getEmail().isEmpty()) {
            customer.setEmail(newCustomer.getEmail());
        }

        if (null != newCustomer.getPhone() && !newCustomer.getPhone().isEmpty()) {
            customer.setPhone(newCustomer.getPhone());
        }

        repository.save(customer);
        redirAttrs.addFlashAttribute("success", "Customer Saved");

        return "redirect:/customers";
    }

    @GetMapping("/delete/{id}")
    public String deleteCustomer(@PathVariable Long id, RedirectAttributes redirAttrs) {

        repository.deleteById(id);
        redirAttrs.addFlashAttribute("success", "Customer Deleted");

        return "redirect:/customers";
    }

}

