package pl.kk.spring.ex6project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kk.spring.ex6project.models.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
