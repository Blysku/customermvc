package pl.kk.spring.ex6project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kk.spring.ex6project.models.User;

public interface UserRepository extends JpaRepository<User, String> {
}
