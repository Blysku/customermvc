package pl.kk.spring.ex6project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class Ex6projectApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ex6projectApplication.class, args);
    }
}
