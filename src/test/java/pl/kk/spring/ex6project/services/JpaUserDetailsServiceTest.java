package pl.kk.spring.ex6project.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JpaUserDetailsServiceTest {

    @Autowired
    private JpaUserDetailsService service;

    @Test
    public void loadUserByUsername() {
        String userName = "user";
        UserDetails userDetails = service.loadUserByUsername(userName);

        assertThat(userDetails.getUsername()).isEqualTo(userName);
    }
}